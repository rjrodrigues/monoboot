package com.lgt.sb.messages.controller;

import com.lgt.sb.common.logging.LoggingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/application")
public class MessagesController {

  private final LoggingService loggingService;

  public MessagesController(LoggingService loggingService) {
    this.loggingService = loggingService;
  }

  @GetMapping
  public String getHello() {
    this.loggingService.info("getHello requested");
    return "hello";
  }
}
