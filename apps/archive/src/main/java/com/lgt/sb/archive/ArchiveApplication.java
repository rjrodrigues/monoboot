package com.lgt.sb.archive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.lgt.sb.*")
public class ArchiveApplication {
  public static void main(String[] args) {
		SpringApplication.run(ArchiveApplication.class, args);
	}

}
