package com.lgt.sb.security.crypto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HashTest {

  @Test
  public void shouldRevertString() {
    assertEquals("tset", Hash.reverse("test"));
  }
}
