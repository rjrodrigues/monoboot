package com.lgt.sb.security.crypto;

public class Hash {

  public static String reverse(String input) {
    StringBuilder toReverse = new StringBuilder();
    toReverse.append(input);
    return toReverse.reverse().toString();
  }
}
