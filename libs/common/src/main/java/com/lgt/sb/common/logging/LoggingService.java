package com.lgt.sb.common.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LoggingService {
  public void info(String message) {
    log.info("info " + message);
  }

  public void debug(String message) {
    log.debug(message);
  }

  public void error(String message) {
    log.error(message);
  }
}
